import React, { useState, useEffect } from "react";
import axios from 'axios';
import _ from 'lodash';
import Fuse from 'fuse.js';

const Filter = () => {
  const [collection, setCollection] = useState([])
  const [category, setCategory] = useState([])

  const fuse = new Fuse(collection,{
    shouldSort: true,
    tokenize: true,
    matchAllTokens: true,
    findAllMatches: true,
    threshold: 0,
    location: 0,
    distance: 0,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: ['item']
  });

  useEffect(() => {
    axios.get(`https://api.publicapis.org/categories`).then(res => {

    const mapObject = _.reduce(res.data , function(obj,data) {
      obj.push({item:data})
      return obj;
     }, []);

    setCollection(mapObject)
    setCategory(mapObject)
    })

    
  }, [])

  const handleChange = (e) => {
    if (e.target.value == ""){
      setCategory(collection)
    }else{
      const results = fuse.search(e.target.value);
      var arr = _.map(results, v => v.item)
      setCategory(arr)
    }
  }


  return (
    <div style={{padding: "50px"}}>
      <input type="text" onChange={handleChange}/>
      <table style={{width:"100%"}}>
        <tr>
          <th>Category</th>
        </tr>
        {
          _.map(category, (each) =>{
            return(
            <tr key={_.get(each, 'item')}>
              <td>{_.get(each, 'item')}</td>
            </tr>
            )
          }
          )
        }
      </table>
    </div>
  );
};

export default Filter;

